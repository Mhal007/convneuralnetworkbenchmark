1. Dla każdej z metod testy efektów uczenia po 1min dla lr:
1, 0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001



Ograniczenie czasowe: 50s
- 3 datasety * 4 metody - 12 tabel i 12 wykresów, po 10 pomiarów (co 5 sekund)
- wykres porównawczy po całości dostępnego czasu (4 słupki - po jednym dla każdej z metod)
- 75% dokładności - potrzebny czas - (4 słupki - po jednym dla każdej z metod)
- Puszczenie do momentu, gdy kilka kolejnym pomiarów nie zwiększy znacznie dotychczasowego maxa - wyniki dla każdej z metod


RMSprop
lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0

Adagrad
lr=0.01, epsilon=1e-08, decay=0.0

Adadelta
lr=1.0, rho=0.95, epsilon=1e-08, decay=0.0

Adam
lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0


