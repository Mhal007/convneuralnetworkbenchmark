import tensorflow as tf

v = tf.Variable(10.)
loss = v * v

optimizerAdadelta = tf.train.AdadeltaOptimizer(1., 0.95, 1e-6)
optimizerAdam     = tf.train.AdamOptimizer(1., 0.95, 1e-6)


optimizer = optimizerAdadelta


train_op = optimizer.minimize(loss)

accum = optimizer.get_slot(v, "accum")  # accumulator of the square gradients
accum_update = optimizer.get_slot(v, "accum_update")  # accumulator of the square updates

sess = tf.Session()
sess.run(tf.global_variables_initializer())

for i in range(100):
    sess.run(train_op)
    print "%.3f \t %.3f \t %.6f" % tuple(sess.run([v, accum, accum_update]))



"""


### Simple example of gradient descent in tensorflow

x = tf.Variable(2, name='x', dtype=tf.float32)
log_x = tf.log(x)
log_x_squared = tf.square(log_x)

optimizer = tf.train.GradientDescentOptimizer(0.5)
train = optimizer.minimize(log_x_squared)

init = tf.global_variables_initializer()

def optimize():
  with tf.Session() as session:
    session.run(init)
    print("starting at", "x:", session.run(x), "log(x)^2:", session.run(log_x_squared))
    for step in range(10):  
      session.run(train)
      print("step", step, "x:", session.run(x), "log(x)^2:", session.run(log_x_squared))
        

optimize()

###

"""



"""


optimizer = optimizerAdam

train_op = optimizer.minimize(loss)

sess = tf.Session()
sess.run(tf.global_variables_initializer())

for i in range(100):
    print sess.run(train_op)




"""



"""


train_op = tf.train.AdamOptimizer(1e-4).minimize(loss)
# Add the ops to initialize variables.  These will include 
# the optimizer slots added by AdamOptimizer().
init_op = tf.global_variables_initializer()

# launch the graph in a session
sess = tf.Session()
# Actually intialize the variables
sess.run(init_op)
# now train your model
for i in range(100):
    print sess.run(train_op)


"""
    

