from __future__ import print_function

import tensorflow as tf
import time

# Lambda for current time
current_milli_time = lambda: int(round(time.time() * 1000))


# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

images = mnist.train.images
labels = mnist.train.labels
images_test = mnist.test.images[:256]
labels_test = mnist.test.labels[:256]

# Network Parameters
n_input   = 28*28 # MNIST data input (img shape: 28*28)
n_classes = 10    # MNIST total classes (0-9 digits)
dropout   = 0.75  # Dropout, probability to keep units


# Parameters
training_iters = 200000
batch_size     = 128
time_limit     = 50  # seconds
logs_interval  = 5   # seconds

    
# Create some wrappers for simplicity
def conv2d(x, W, b, strides=1):
    # Conv2D wrapper, with bias and relu activation
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    x = tf.nn.bias_add(x, b)
    return tf.nn.relu(x)


def maxpool2d(x, k=2):
    # MaxPool2D wrapper
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')




optimizers         = ["Adam", "Adadelta", "Adagrad", "GradientDescent", "RMSProp"]
activation_methods = ["relu", "relu6", "crelu", "elu", "softsign", "softplus", "sigmoid", "tanh"]
learning_rates     = [1, 0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001]
optimizer          = None



# Create model
def conv_net(x, weights, biases, dropout, activation_index):
    # Reshape input picture
    x = tf.reshape(x, shape=[-1, 28, 28, 1])
    # Convolution Layer
    conv1 = conv2d(x, weights['wc1'], biases['bc1'])
    # Max Pooling (down-sampling)
    conv1 = maxpool2d(conv1, k=2)

    # Convolution Layer
    conv2 = conv2d(conv1, weights['wc2'], biases['bc2'])
    # Max Pooling (down-sampling)
    conv2 = maxpool2d(conv2, k=2)

    # Fully connected layer
    # Reshape conv2 output to fit fully connected layer input
    fc1 = tf.reshape(conv2, [-1, weights['wd1'].get_shape().as_list()[0]])
    fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])

    if activation_index == 0:
      fc1 = tf.nn.relu     (fc1)
    if activation_index == 1:
      fc1 = tf.nn.relu6    (fc1)
    if activation_index == 2:
      fc1 = tf.nn.crelu    (fc1)
    if activation_index == 3:
      fc1 = tf.nn.elu      (fc1)
    if activation_index == 4:
      fc1 = tf.nn.softsign (fc1)
    if activation_index == 5:
      fc1 = tf.nn.softplus (fc1)
    if activation_index == 6:
      fc1 = tf.nn.sigmoid  (fc1)
    if activation_index == 7:
      fc1 = tf.nn.tanh     (fc1)

    
    # Apply Dropout
    fc1 = tf.nn.dropout(fc1, dropout)

    # Output, class prediction
    out = tf.add(tf.matmul(fc1, weights['out']), biases['out'])
    return out


f = open('results_new', 'w')

for optimizer_index      in range(0, len(optimizers)):
    for activation_index in range(0, len(activation_methods)):
        for lr_index     in range(0, len(learning_rates)):

            #optimizer_index = 2    #Optimizer     - AdadeltaOptimizer
            #lr_index     = 2    #Learning rate - 1


            print("######## " + optimizers[optimizer_index] + ";" + activation_methods[activation_index] + ";" + "{:.6f}".format(learning_rates[lr_index]))
            f.write(optimizers[optimizer_index] + ";" + activation_methods[activation_index] + ";" + "{:.6f}".format(learning_rates[lr_index]) + "\n")

            current_learning_rate = learning_rates[lr_index]

            # tf Graph input
            x         = tf.placeholder(tf.float32, [None, n_input])
            y         = tf.placeholder(tf.float32, [None, n_classes])
            keep_prob = tf.placeholder(tf.float32) #dropout (keep probability)

            # Exception for crelu
            if (activation_index == 2):
                depth = 2048
            else:
                depth = 1024    

            # Store layers weight & bias
            weights = {
                # 5x5 conv, 1 input, 32 outputs
                'wc1': tf.Variable(tf.random_normal([5, 5, 1, 32])),
                # 5x5 conv, 32 inputs, 64 outputs
                'wc2': tf.Variable(tf.random_normal([5, 5, 32, 64])),
                # fully connected, 7*7*64 inputs, 1024 outputs
                'wd1': tf.Variable(tf.random_normal([7*7*64, 1024])),
                # 1024 inputs, 10 outputs (class prediction)
                'out': tf.Variable(tf.random_normal([depth, n_classes]))
            }
            biases = {
                'bc1': tf.Variable(tf.random_normal([32])),
                'bc2': tf.Variable(tf.random_normal([64])),
                'bd1': tf.Variable(tf.random_normal([1024])),
                'out': tf.Variable(tf.random_normal([n_classes]))
            }

            # Construct model
            pred = conv_net(x, weights, biases, keep_prob, activation_index)

            # Define loss and optimizer
            cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))

            # Evaluate model
            correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
            accuracy     = tf.reduce_mean(tf.cast(correct_pred, tf.float32))



            # Autoencoder train methods
            if optimizer_index == 0:
                optimizer = tf.train.AdamOptimizer            (learning_rate=current_learning_rate).minimize(cost)
            if optimizer_index == 1:
                optimizer = tf.train.AdadeltaOptimizer        (learning_rate=current_learning_rate).minimize(cost)
            if optimizer_index == 2:
                optimizer = tf.train.AdagradOptimizer         (learning_rate=current_learning_rate).minimize(cost)
            if optimizer_index == 3:
                optimizer = tf.train.GradientDescentOptimizer (learning_rate=current_learning_rate).minimize(cost)
            if optimizer_index == 4:
                optimizer = tf.train.RMSPropOptimizer         (learning_rate=current_learning_rate).minimize(cost)


            # Initializing the variables
            init = tf.global_variables_initializer()

            # Launching the graph
            with tf.Session() as sess:
                sess.run(init)
                
                startTime   = current_milli_time()
                logsDone    = 0
                
                while logsDone*logs_interval < time_limit:
                    batch_x, batch_y = mnist.train.next_batch(batch_size)
                    sess.run(optimizer, feed_dict={x: batch_x, y: batch_y, keep_prob: dropout})    #backprop

                    currentTime = current_milli_time()
                    if currentTime-startTime > logs_interval*1000*(logsDone+1):
                        curr_train_acc = sess.run(accuracy, feed_dict={x: batch_x, y: batch_y, keep_prob: 1.})
                        curr_test_acc  = sess.run(accuracy, feed_dict={x: images_test, y: labels_test, keep_prob: 1.})
                        logsDone=logsDone+1

                        print("######## " + str(logsDone*logs_interval) + ";" + "{:.2f}".format(curr_test_acc))
                        f.write(str(logsDone*logs_interval) + ";" + "{:.2f}".format(curr_test_acc) + "\n")


                            
            print("Optimization Finished!")
            tf.reset_default_graph()

##end of indentation (x2) 

f.close()
